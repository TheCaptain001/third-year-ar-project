<?php
session_start();
include "config.php";
if(!isset($_SESSION['uname'])){
  header('location: login.php');
}
?>
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <title>Login</title>
        <script src="js/scripts.js"></script>
        <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script>
        <script src="js/ar.js"></script>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script>
            var place;
            AFRAME.registerComponent('markerhandler', {
                tick: function () {
                    var markers = document.querySelectorAll('a-marker');

                    for (var i = 0; i < markers.length; i++) {
                        if (markers[i].object3D.visible == true) {
                            //console.log("gogogo");
                            place = markers[i].getAttribute('id');
                            console.log(place);
                        }
                    }
                }
            });
        </script>
    </head>

    <body>
        <?php include "floor_data.php" ?>
        <?php include "timetable_v2.php" ?>
        <script>
            /*
            var d = new Date();
            var day = d.getDay();
            var hour = d.getHours();
            var min = d.getMinutes();
            document.getElementById("day").innerHTML = day;
            document.getElementById("hour").innerHTML = hour;
            document.getElementById("minute").innerHTML = min;
            */
        </script>

        <div class="wrapper">
            <button onclick="logout()" class="button">Logout</button>
        </div>
        <div class="wrapper">
            <button onclick="dissapear()" class="button">Guest</button>
        </div>

        <a-scene id="scene" embedded arjs='trackingMethod: best; debugUIEnabled: false;' vr-mode-ui="enabled: false">

            <a-marker id="049" preset='custom' type='pattern' url='patterns/a.patt'>
                <a-plane id="lesson_board" title="049" scale="1 1 1" position="0 0 0" opacity="0.5" rotation="-90 0 0" width="2" height="2"
                    color="yellow" markerhandler>
                    <a-text id=lesson_title value="null" align="center" color="black" position="0 0.5 0.1"></a-text>
                    <a-text id=lesson_room value="null" align="center" color="black" position="0 -0.5 0.1"></a-text>
                </a-plane>
                <a-plane id="lesson_board" title="049" scale="1 1 1" position="0 0 -3" opacity="0.5" rotation="-90 0 0" width="2" height="2"
                    color="yellow" markerhandler>
                    <a-text id=aaa value="null" align="center" color="black" position="0 0.5 0.1"></a-text>
                    <a-text id=bbb value="null" align="center" color="black" position="0 0 0.1"></a-text>
                    <a-text id=ccc value="null" align="center" color="black" position="0 -0.5 0.1"></a-text>
                </a-plane>
            </a-marker>

            <a-entity camera></a-entity>
            <a-entity cursor="fuse: false; rayOrigin: mouse"><!--makes the touch work-->

        </a-scene>

        <!--idea is that time would be the deciding factor
                substituting numbers instead for test puroses-->


        <script>
            //triangle entity set ups to be used later
            var right_tri = document.createElement('a-triangle');
                right_tri.setAttribute('color', 'red');
                right_tri.setAttribute('position', {
                    x: 0,
                    y: 0,
                    z: 1.5
                });
                right_tri.setAttribute('rotation', {
                    x: 0,
                    y: 0,
                    z: -90
                });
     
                var left_tri = document.createElement('a-triangle');
                left_tri.setAttribute('color', 'red');
                left_tri.setAttribute('position', {
                    x: 0,
                    y: 0,
                    z: 1.5
                });
                left_tri.setAttribute('rotation', {
                    x: 0,
                    y: 0,
                    z: 90
                });
    
                var up_tri = document.createElement('a-triangle');
                up_tri.setAttribute('color', 'red');
                up_tri.setAttribute('position', {
                    x: 0,
                    y: 0,
                    z: 1.5
                });
    
                var down_tri = document.createElement('a-triangle');
                down_tri.setAttribute('color', 'red');
                down_tri.setAttribute('position', {
                    x: 0,
                    y: 0,
                    z: 1.5
                });
                down_tri.setAttribute('rotation', {
                    x: 0,
                    y: 0,
                    z: 180
                });

            var comp;
            
            var first_floor, second_floor, third_floor;
            first_floor = '<?php echo $_SESSION['first_floor'] ?>';
            second_floor = '<?php echo $_SESSION['second_floor'] ?>';
            third_floor = '<?php echo $_SESSION['third_floor'] ?>';

      
            var sceneEl = document.querySelector('#scene');
            var a_a_a = sceneEl.querySelector('#aaa');
            var b_b_b = sceneEl.querySelector('#bbb');
            var c_c_c = sceneEl.querySelector('#ccc');
            a_a_a.setAttribute('text', 'value', (first_floor));
            b_b_b.setAttribute('text', 'value', (second_floor));
            c_c_c.setAttribute('text', 'value', (third_floor));

            var l_board = document.querySelector('#lesson_board');

            var lesson_1_start = '<?php echo $result_array[0]['start'] ?>';
            var lesson_2_start = '<?php echo $result_array[1]['start'] ?>';
            var lesson_3_start = '<?php echo $result_array[2]['start'] ?>';

            time_of_day = '10';
            
            if(time_of_day == lesson_1_start){
                
                console.log("time = 12");
                var lesson_1_title = '<?php echo $result_array[0]['title'] ?>';
                var lesson_1_room = '<?php echo $result_array[0]['room'] ?>';
                var a = sceneEl.querySelector('#lesson_title');
                var b = sceneEl.querySelector('#lesson_room');
                a.setAttribute('value', (lesson_1_title));
                b.setAttribute('value', (lesson_1_room));

                l_board.addEventListener("click", function (e) {
                        console.log("l_board");
                        if(lesson_1_room = 'MLT'){
                            comp = '001';
                        }
                        console.log(comp);
                        if (comp < place) {
                            l_board.appendChild(left_tri);
                            setTimeout(function () {
                                l_board.removeChild(left_tri);
                                comp = null;
                            }, 3000);
                        } else {
                            console.log("BROKEN");
                        }
    
                    }, false);



            }else{
                console.log("not good");
            }
            
            if(time_of_day == lesson_2_start){
                
                console.log("time = 16");
                var lesson_1_title = '<?php echo $result_array[1]['title'] ?>';
                var lesson_1_room = '<?php echo $result_array[1]['room'] ?>';
                var a = sceneEl.querySelector('#lesson_title');
                var b = sceneEl.querySelector('#lesson_room');
                a.setAttribute('value', (lesson_1_title));
                b.setAttribute('value', (lesson_1_room));

                l_board.addEventListener("click", function (e) {
                        console.log("l_board");
                        if(lesson_1_room = 'MLT'){
                            comp = '001';
                        }
                        console.log(comp);
                        if (comp < place) {
                            l_board.appendChild(left_tri);
                            setTimeout(function () {
                                l_board.removeChild(left_tri);
                                comp = null;
                            }, 3000);
                        } else {
                            console.log("BROKEN");
                        }
    
                    }, false);



            }else{
                console.log("not good");
            }

        if(time_of_day == lesson_3_start){
                
                console.log("time = 10");
                var lesson_1_title = '<?php echo $result_array[2]['title'] ?>';
                var lesson_1_room = '<?php echo $result_array[2]['room'] ?>';
                var a = sceneEl.querySelector('#lesson_title');
                var b = sceneEl.querySelector('#lesson_room');
                a.setAttribute('value', (lesson_1_title));
                b.setAttribute('value', (lesson_1_room));

                l_board.addEventListener("click", function (e) {
                        console.log("l_board");
                        if(lesson_1_room = '207'){
                            comp = '101';
                        }
                        console.log(comp);
                        if (comp > place) {
                            l_board.appendChild(up_tri);
                            left_tri.setAttribute('position', {
                                x: 0,
                                y: -2,
                                z: 1.5
                            });
                            l_board.appendChild(left_tri);
                            setTimeout(function () {
                                l_board.removeChild(up_tri);
                                l_board.removeChild(left_tri);
                                comp = null;
                            }, 3000);
                        } else {
                            console.log("BROKEN");
                        }
    
                    }, false);



            }else{
                console.log("not good");
            }
        </script>


    </body>

    </html>