<?php
//essential
session_start();
 ?>
 
  <html>

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <title>Login</title>
    <script src="js/scripts.js"></script>
    <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script>
    <script src="js/ar.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>

  <body>
    <div class="wrapper" id="btnOne">
      <button onclick="login(1)" class="button">Login</button>
    </div>
    <div class="wrapper" id="btnTwo">
      <button onclick="dissapear()" class="button">Guest</button>
    </div>

    <div class="wrapper" id="loginForm">
      <form method="post">
        <?php//crosschecks info against database, then retrieves to be used as vars
      include "config.php";
      if(isset($_POST['uname']) && isset($_POST['pWord'])){
        $uname = $_POST['uname'];
        $pWord = $_POST['pWord'];
        //next line is a query
        $result = $mysqli->query("select * from users where uname='$uname' and pWord='$pWord'");
        $row = $result->fetch_assoc();
        $id = $row['id'];
        $username = $row['uname']; 
        $password = $row['pWord'];
        $email_address = $row['email'];
        $year_of_study = $row['year'];

        if($username==$uname && $pWord==$password){
          //stores info as session data, so can be used across pages
          $_SESSION['uname']=$username;
          $_SESSION['id']=$id;==
          $_SESSION['year']=$year_of_study;
          ?>
          <script>
            window.location.href = 'index.php'
          </script>
          <?php
        }
      }
      ?>
            <input type="text" name="uname" placeholder="Username">
            <br>
            <input type="password" name="pWord" placeholder="Password">
            <br>
            <button class="button" type="submit">Submit</button>
            <br>
            <a href="signup_form_student.html" class="button">New Account</a>
      </form>
    </div>
    <div>
      <!--below handles live camera feed-->
      <a-scene embedded arjs='trackingMethod: best; debugUIEnabled: false;' vr-mode-ui="enabled: false">

        <a-entity camera></a-entity>

      </a-scene>
    </div>
  </body>

  </html>